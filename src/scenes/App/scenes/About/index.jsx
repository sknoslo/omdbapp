import React from 'react';

export default () => (
  <div>
    <section className="hero is-primary">
      <div className="hero-body">
        <div className="container">
          <h1 className="title is-1">About</h1>
        </div>
      </div>
    </section>
    <section className="section">
      <div className="container">
        <div className="content">
          <p>
            This is a demo of the OMDb Api. Created with:
          </p>
          <ul>
            <li>
              <a href="http://omdbapi.com">The OMDb Api</a>
            </li>
            <li>
              <a href="https://facebook.github.io/react/">React</a>
            </li>
            <li>
              <a href="http://redux.js.org">Redux</a>
            </li>
            <li>
              <a href="https://reacttraining.com/react-router/">React Router</a>
            </li>
            <li>
              <a href="http://bulma.io/">Bulma</a>
            </li>
          </ul>
        </div>
      </div>
    </section>
  </div>
);
