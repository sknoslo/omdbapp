import React, { Component, PropTypes } from 'react';

export default class Poster extends Component {
  static propTypes = {
    src: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }

  state = {
    shouldDisplayImage: true,
  }

  handleImageLoadError = () => {
    this.setState({
      shouldDisplayImage: false,
    });
  }

  render() {
    if (!this.state.shouldDisplayImage) {
      return null;
    }

    const { src, title } = this.props;

    return (
      <figure className="image">
        <img src={src} alt={title} onError={this.handleImageLoadError} />
      </figure>
    );
  }
}
