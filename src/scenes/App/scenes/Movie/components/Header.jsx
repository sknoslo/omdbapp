import React, { PropTypes } from 'react';

const Header = ({ title, year, type }) => (
  <section className="hero is-primary">
    <div className="hero-body">
      <div className="container">
        <h1 className="title is-1">{title} ({year} {type})</h1>
      </div>
    </div>
  </section>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
  year: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};

export default Header;
