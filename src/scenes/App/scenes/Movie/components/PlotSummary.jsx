import React, { PropTypes } from 'react';

const PlotSummary = ({ plot }) => (
  <div>
    <h3 className="title is-3">Plot Summary</h3>
    <div className="content">
      <p>{plot}</p>
    </div>
  </div>
);

PlotSummary.propTypes = {
  plot: PropTypes.string.isRequired,
};

export default PlotSummary;
