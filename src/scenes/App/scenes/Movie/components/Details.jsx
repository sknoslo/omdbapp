import React, { PropTypes } from 'react';
import Poster from './Poster';
import PlotSummary from './PlotSummary';

const Details = ({ posterSrc, title, plot }) => (
  <section className="section">
    <div className="container">
      <div className="columns">
        <div className="column is-3">
          <Poster src={posterSrc} title={title} />
        </div>
        <div className="column is-9">
          <PlotSummary plot={plot} />
        </div>
      </div>
    </div>
  </section>
);

Details.propTypes = {
  posterSrc: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  plot: PropTypes.string.isRequired,
};

export default Details;
