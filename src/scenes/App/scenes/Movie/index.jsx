import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchMovieIfNeeded, makeGetMovieSelector } from 'store/modules/movies';
import Loader from 'components/Loader';
import Error from 'components/Error';
import Header from './components/Header';
import Details from './components/Details';

class Movie extends Component {
  static propTypes = {
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string.isRequired,
    movieData: PropTypes.shape({
      Title: PropTypes.string,
    }).isRequired,
    fetchMovie: PropTypes.func.isRequired,
  }

  componentDidMount() {
    this.props.fetchMovie();
  }

  render() {
    const {
      isLoading,
      error,
      errorMessage,
      movieData,
    } = this.props;

    if (error) {
      return <Error message={errorMessage} />;
    }

    return (
      <Loader isLoading={isLoading}>
        {() => (
          <div>
            <Header
              title={movieData.Title}
              year={movieData.Year}
              type={movieData.Type}
            />
            <Details
              posterSrc={movieData.Poster}
              title={movieData.Title}
              plot={movieData.Plot}
            />
          </div>
        )}
      </Loader>
    );
  }
}

function mapStateToProps(state, ownProps) {
  const { imdbID } = ownProps.match.params;

  const getMovie = makeGetMovieSelector(imdbID);

  return getMovie(state);
}

function mapDispatchToProps(dispatch, ownProps) {
  const { imdbID } = ownProps.match.params;

  return {
    fetchMovie: () => dispatch(fetchMovieIfNeeded(imdbID)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Movie);
