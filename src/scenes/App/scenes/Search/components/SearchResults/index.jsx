import React, { PropTypes } from 'react';
import Error from 'components/Error';
import MovieResult from './components/MovieResult';
import styles from './index.scss';

const SearchResults = ({
  searchTerm,
  error,
  errorMessage,
  movies,
}) => {
  let heading;

  if (error) {
    return <Error message={errorMessage} />;
  }

  if (searchTerm === '') {
    heading = (
      <h1 className={styles.heading}>
        Try searching for a movie title.
      </h1>
    );
  } else {
    heading = (
      <h1 className={styles.heading}>
        Showing results for &quot;{searchTerm}&quot;.
      </h1>
    );
  }

  return (
    <section className="section">
      <div className="columns">
        <div className="column is-8-desktop is-offset-2-desktop has-text-centered">
          {heading}
          {movies.map(movie => (
            <MovieResult
              key={movie.imdbID}
              title={movie.Title}
              poster={movie.Poster}
              type={movie.Type}
              year={movie.Year}
              imdbID={movie.imdbID}
            />
          ))}
        </div>
      </div>
    </section>
  );
};

SearchResults.propTypes = {
  searchTerm: PropTypes.string.isRequired,
  error: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string.isRequired,
  movies: PropTypes.arrayOf(PropTypes.shape({
    Poster: PropTypes.string,
    Title: PropTypes.string,
    Type: PropTypes.string,
    Year: PropTypes.string,
    imdbID: PropTypes.string,
  })).isRequired,
};

export default SearchResults;
