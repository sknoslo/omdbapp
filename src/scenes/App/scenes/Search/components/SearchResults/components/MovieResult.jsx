import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router-dom';
import styles from './MovieResult.scss';

export default class MovieResult extends Component {
  static propTypes = {
    poster: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    year: PropTypes.string.isRequired,
    imdbID: PropTypes.string.isRequired,
  }

  state = {
    shouldDisplayImage: true,
  }

  handleImageLoadError = () => {
    this.setState({
      shouldDisplayImage: false,
    });
  }

  render() {
    const {
      poster,
      title,
      type,
      year,
      imdbID,
    } = this.props;

    return (
      <Link to={`/movies/${imdbID}`} className="box">
        <article className="media">
          <div className="media-left">
            <figure className={styles.poster}>
              {this.state.shouldDisplayImage ?
                <img src={poster} alt={title} onError={this.handleImageLoadError} /> :
                <div className={styles.imgPlaceholder} />
              }
            </figure>
          </div>
          <div className="media-content">
            <h3 className="title is-3">{title} ({type})</h3>
            <h5 className="subtitle is-5">{year}</h5>
          </div>
        </article>
      </Link>
    );
  }
}
