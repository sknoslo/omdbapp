import React, { Component, PropTypes } from 'react';
import Button from 'components/Button';
import styles from './SearchBar.scss';

class SearchBar extends Component {
  static propTypes = {
    isLoading: PropTypes.bool.isRequired,
    searchInput: PropTypes.string.isRequired,
    onSearchInputChanged: PropTypes.func.isRequired,
    onSearch: PropTypes.func.isRequired,
  }

  handleSearch = (e) => {
    e.preventDefault();

    const { onSearch, searchInput } = this.props;

    this.input.blur();

    return onSearch(searchInput);
  }

  render() {
    const {
      isLoading,
      searchInput,
      onSearchInputChanged,
    } = this.props;

    return (
      <form onSubmit={this.handleSearch}>
        <div className="field is-grouped">
          <p className="control is-expanded">
            <input
              type="search"
              ref={(input) => { this.input = input; }}
              className="input is-medium"
              placeholder="search..."
              value={searchInput}
              onChange={onSearchInputChanged}
            />
          </p>
          <p className="control is-hidden-mobile">
            <Button
              className={styles.searchButton}
              theme="is-primary"
              size="is-medium"
              loading={isLoading}
            >
              Search
            </Button>
          </p>
        </div>
      </form>
    );
  }
}

export default SearchBar;
