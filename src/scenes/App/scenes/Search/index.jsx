import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import {
  updateSearchInput,
  searchIfNeeded,
  getSearchInput,
  getSearchTerm,
  getSearchResults,
} from 'store/modules/search';
import Loader from 'components/Loader';
import SearchResults from './components/SearchResults';
import SearchBar from './components/SearchBar';

const Movies = ({
  searchInput,
  searchTerm,
  searchResults,
  handleSearchInputChanged,
  handleSearch,
}) => (
  <div>
    <section className="hero">
      <div className="hero-body">
        <div className="columns">
          <div className="column is-8-desktop is-offset-2-desktop has-text-centered">
            <h1 className="title is-1">Search OMDb</h1>
            <SearchBar
              isLoading={searchResults.isLoading}
              searchInput={searchInput}
              onSearchInputChanged={handleSearchInputChanged}
              onSearch={handleSearch}
            />
          </div>
        </div>
      </div>
    </section>
    <Loader isLoading={searchResults.isLoading}>
      {() => (
        <SearchResults
          searchTerm={searchTerm}
          {...searchResults}
        />
      )}
    </Loader>
  </div>
);

Movies.propTypes = {
  searchInput: PropTypes.string.isRequired,
  searchTerm: PropTypes.string.isRequired,
  searchResults: PropTypes.shape({
    isLoading: PropTypes.bool,
  }).isRequired,
  handleSearchInputChanged: PropTypes.func.isRequired,
  handleSearch: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    searchInput: getSearchInput(state),
    searchTerm: getSearchTerm(state),
    searchResults: getSearchResults(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleSearchInputChanged: (e) => {
      dispatch(updateSearchInput(e.target.value));
    },
    handleSearch: (searchTerm) => {
      dispatch(searchIfNeeded(searchTerm));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Movies);
