import React from 'react';
import ErrorIcon from 'components/ErrorIcon';

const NoMatch = () => (
  <section className="section">
    <div className="container has-text-centered">
      <ErrorIcon size={96} />
      <h1 className="title is-1">Page not found.</h1>
    </div>
  </section>
);

export default NoMatch;
