import React, { PropTypes } from 'react';
import styles from './ErrorIcon.scss';

const ErrorIcon = ({ size }) => (
  <svg
    className={styles.errorIcon}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 192 192"
    width={size}
    height={size}
  >
    <g>
      <path
        d="M 39.263672,23.269531 A 16.0016,16.0016 0 0 0 28.117188,50.746094 L 141.25391,163.88281 a 16.0016,16.0016 0 1 0 22.6289,-22.6289 L 50.746094,28.117188 A 16.0016,16.0016 0 0 0 39.263672,23.269531 Z"
      />
      <path
        d="m 152.25391,23.277344 a 16.0016,16.0016 0 0 0 -11,4.839844 L 28.117188,141.25391 a 16.0016,16.0016 0 1 0 22.628906,22.6289 L 163.88281,50.746094 a 16.0016,16.0016 0 0 0 -11.6289,-27.46875 z"
      />
    </g>
  </svg>
);

ErrorIcon.propTypes = {
  size: PropTypes.number,
};

ErrorIcon.defaultProps = {
  size: 192,
};

export default ErrorIcon;
