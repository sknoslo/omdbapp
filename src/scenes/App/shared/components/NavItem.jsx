import React, { PropTypes } from 'react';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';

const NavItem = ({ isTab, isBrand, ...rest }) => (
  <NavLink
    className={classNames('nav-item', { 'is-tab': isTab }, { 'is-brand': isBrand })}
    activeClassName="is-active"
    {...rest}
  />
);

NavItem.propTypes = {
  isTab: PropTypes.bool,
  isBrand: PropTypes.bool,
};

NavItem.defaultProps = {
  isTab: false,
  isBrand: false,
};

export default NavItem;
