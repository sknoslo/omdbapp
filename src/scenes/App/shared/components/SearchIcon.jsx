import React, { PropTypes } from 'react';

const SearchIcon = ({ className }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    width="32"
    height="32"
  >
    <g>
      <path
        d="M 19 2 A 11 11 0 0 0 8 13 A 11 11 0 0 0 9.9453125 19.226562 L 1.7363281 27.435547 L 4.5644531 30.263672 L 12.767578 22.060547 A 11 11 0 0 0 19 24 A 11 11 0 0 0 30 13 A 11 11 0 0 0 19 2 z M 19 6 A 7 7 0 0 1 26 13 A 7 7 0 0 1 19 20 A 7 7 0 0 1 12 13 A 7 7 0 0 1 19 6 z " // eslint-disable-line max-len
      />
    </g>
  </svg>
);

SearchIcon.propTypes = {
  className: PropTypes.string,
};

SearchIcon.defaultProps = {
  className: null,
};

export default SearchIcon;
