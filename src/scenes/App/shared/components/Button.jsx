import React, { PropTypes } from 'react';
import classNames from 'classnames';

const Button = ({
  className,
  type,
  size,
  theme,
  outlined,
  inverted,
  loading,
  children,
  ...buttonProps
}) => {
  const combinedClassName = classNames(
    className,
    'button',
    size,
    theme,
    { 'is-outlined': outlined },
    { 'is-inverted': inverted },
    { 'is-loading': loading },
  );

  return (
    <button
      type={type}
      className={combinedClassName}
      {...buttonProps}
    >
      {children}
    </button>
  );
};

Button.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  size: PropTypes.oneOf([
    'is-small',
    'is-medium',
    'is-large',
  ]),
  theme: PropTypes.oneOf([
    'is-primary',
    'is-info',
    'is-success',
    'is-warning',
    'is-danger',
    'is-link',
  ]),
  outlined: PropTypes.bool,
  inverted: PropTypes.bool,
  loading: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

Button.defaultProps = {
  className: null,
  type: 'submit',
  size: null,
  theme: null,
  outlined: false,
  inverted: false,
  loading: false,
};

export default Button;
