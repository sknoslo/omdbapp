import React, { PropTypes } from 'react';
import styles from './Spinner.scss';

const Spinner = ({ size, duration }) => (
  <svg
    className={styles.spinner}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 192 192"
    width={size}
    height={size}
  >
    <g>
      <path
        className={styles.bg}
        fillOpacity={0.25}
        d="M 96 0 A 96 96 0 0 0 0 96 A 96 96 0 0 0 96 192 A 96 96 0 0 0 192 96 A 96 96 0 0 0 96 0 z M 96 32 A 64 64 0 0 1 160 96 A 64 64 0 0 1 96 160 A 64 64 0 0 1 32 96 A 64 64 0 0 1 96 32 z "
      />
      <path
        className={styles.fg}
        d="M 94.597656 0.009765625 A 96 96 0 0 0 80.236328 1.4238281 L 85.486328 32.921875 A 64 64 0 0 1 106.50781 32.949219 L 111.76953 1.3828125 A 96 96 0 0 0 94.597656 0.009765625 z "
      >
        <animateTransform
          attributeType="xml"
          attributeName="transform"
          type="rotate"
          from="0 96 96"
          to="360 96 96"
          dur={duration}
          repeatCount="indefinite"
        />
      </path>
    </g>
  </svg>
);

Spinner.propTypes = {
  size: PropTypes.number,
  duration: PropTypes.string,
};

Spinner.defaultProps = {
  size: 192,
  duration: '0.7s',
};

export default Spinner;
