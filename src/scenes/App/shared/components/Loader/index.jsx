import React, { PropTypes } from 'react';
import Spinner from './components/Spinner';
import styles from './index.scss';

const Loader = ({ isLoading, children }) => {
  if (isLoading) {
    return (
      <div className={styles.spinnerCenter}>
        <Spinner size={96} />
      </div>
    );
  }

  return children();
};

Loader.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  children: PropTypes.func.isRequired,
};

export default Loader;
