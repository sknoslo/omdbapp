import React, { PropTypes } from 'react';
import ErrorIcon from 'components/ErrorIcon';

const NotFound = ({ message }) => (
  <section className="section">
    <div className="container has-text-centered">
      <ErrorIcon size={96} />
      <h1 className="title is-1">
        {message}
      </h1>
    </div>
  </section>
);

NotFound.propTypes = {
  message: PropTypes.string,
};

NotFound.defaultProps = {
  message: 'Oops. Something went wrong.',
};

export default NotFound;
