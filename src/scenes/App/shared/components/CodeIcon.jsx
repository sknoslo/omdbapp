import React, { PropTypes } from 'react';

const CodeIcon = ({ className }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 32 32"
    width="32"
    height="32"
  >
    <g>
      <path
        d="M 11.606602,5.393398 1,16 l 0.7071068,0.707107 2.1213203,2.12132 7.7781749,7.778175 2.828427,-2.828427 L 6.6568542,16 14.435029,8.221825 Z" // eslint-disable-line max-len
      />
      <path
        d="M 20.393398,5.393398 31,16 l -0.707107,0.707107 -2.12132,2.12132 -7.778175,7.778175 L 17.564971,23.778175 25.343146,16 17.564971,8.221825 Z" // eslint-disable-line max-len
      />
    </g>
  </svg>
);

CodeIcon.propTypes = {
  className: PropTypes.string,
};

CodeIcon.defaultProps = {
  className: null,
};

export default CodeIcon;
