import React from 'react';
import {
  HashRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import NavItem from 'components/NavItem';
import CodeIcon from 'components/CodeIcon';
import Search from './scenes/Search';
import About from './scenes/About';
import Movie from './scenes/Movie';
import NoMatch from './scenes/NoMatch';
import styles from './index.scss';

export default () => (
  <Router>
    <div className={styles.page}>
      <nav className="nav">
        <div className="container">
          <div className="nav-left">
            <NavItem isBrand to="/">
              <h1 className="title">
                OMDb
              </h1>
            </NavItem>
            <NavItem isTab exact to="/">Search</NavItem>
            <NavItem isTab to="/about">About</NavItem>
          </div>
        </div>
      </nav>
      <div className={styles.pageContent}>
        <Switch>
          <Route exact path="/" component={Search} />
          <Route path="/about" component={About} />
          <Route path="/movies/:imdbID" component={Movie} />
          <Route component={NoMatch} />
        </Switch>
      </div>
      <footer className="footer">
        <div className="container">
          <div className="content has-text-centered">
            <CodeIcon className={styles.codeIcon} />
            {' by Steffen Olson.'}
          </div>
        </div>
      </footer>
    </div>
  </Router>
);
