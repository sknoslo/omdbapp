import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from 'redux-logger';
import reducer from './reducer';

export default function configureStore() {
  return createStore(
    reducer,
    undefined,
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware,
    ),
  );
}
