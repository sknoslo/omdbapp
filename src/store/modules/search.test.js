import moxios from 'moxios';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import reducer, {
  initialState,
  SEARCH_INPUT_UPDATE,
  SEARCH_TERM_UPDATE,
  SEARCH_REQUEST,
  SEARCH_SUCCESS,
  SEARCH_FAILURE,
  searchIfNeeded,
  updateSearchInput,
} from './search';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('sync actions', () => {
  it('should create an action to update the search input', () => {
    const searchInput = 'Some Movie';
    const expectedAction = {
      type: SEARCH_INPUT_UPDATE,
      payload: searchInput,
    };

    expect(updateSearchInput(searchInput)).toEqual(expectedAction);
  });
});

describe('async actions', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  it('creates SEARCH_TERM_UPDATE SEARCH_REQUEST and SEARCH_SUCCESS actions if search is needed', () => {
    const searchInput = 'Some Movie';

    const response = {
      Response: 'True',
      Search: ['fake', 'search', 'results'],
      totalResults: '30',
    };

    moxios.stubRequest(/omdbapi/, {
      status: 200,
      response,
    });

    const expectedActions = [
      {
        type: SEARCH_TERM_UPDATE,
        payload: searchInput,
      },
      {
        type: SEARCH_REQUEST,
        payload: {
          searchTerm: searchInput,
        },
      },
      {
        type: SEARCH_SUCCESS,
        payload: {
          searchTerm: searchInput,
          data: response,
        },
      },
    ];

    const store = mockStore({ search: initialState });
    return store.dispatch(searchIfNeeded(searchInput))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });

  it('does not search if search is not needed', () => {
    const searchInput = 'Fake Movie';

    const store = mockStore({
      search: {
        bySearchTerm: {
          [searchInput]: {
            isLoading: false,
            error: false,
          },
        },
      },
    });

    const expectedActions = [
      {
        type: SEARCH_TERM_UPDATE,
        payload: searchInput,
      },
    ];

    return store.dispatch(searchIfNeeded(searchInput))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });

  it('does not search if input is empty', () => {
    const store = mockStore({ search: initialState });

    const expectedActions = [
      {
        type: SEARCH_TERM_UPDATE,
        payload: '',
      },
    ];

    return store.dispatch(searchIfNeeded('                         '))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });

  it('creates a SEARCH_FAILURE action when movie is not found', () => {
    const searchInput = 'Unfindable Movie';
    const errorMessage = 'Movie not found!';

    // The omdbapi does return 200 even when no movie was found.
    moxios.stubRequest(/omdbapi/, {
      status: 200,
      response: {
        Response: 'False',
        Error: errorMessage,
      },
    });

    const expectedActions = [
      {
        type: SEARCH_TERM_UPDATE,
        payload: searchInput,
      },
      {
        type: SEARCH_REQUEST,
        payload: {
          searchTerm: searchInput,
        },
      },
      {
        type: SEARCH_FAILURE,
        error: true,
        payload: {
          searchTerm: searchInput,
          error: new Error(errorMessage),
        },
      },
    ];

    const store = mockStore({ search: initialState });

    return store.dispatch(searchIfNeeded(searchInput))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });
});

describe('search reducer', () => {
  it('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      searchInput: '',
      searchTerm: '',
      bySearchTerm: {},
    });
  });

  it('should handle a search request', () => {
    const searchTerm = 'Some Movie';
    expect(reducer(undefined, {
      type: SEARCH_REQUEST,
      payload: { searchTerm },
    })).toEqual({
      bySearchTerm: {
        [searchTerm]: {
          isLoading: true,
          error: false,
          errorMessage: '',
          movies: [],
          totalResults: 0,
        },
      },
      searchTerm: '',
      searchInput: '',
    });
  });

  it('should handle a successful fetch', () => {
    const searchTerm = 'Some Movie';
    const fakeData = { Search: ['fake', 'movies'], totalResults: '42' };

    expect(reducer({
      bySearchTerm: {
        [searchTerm]: {
          isLoading: true,
          error: false,
          errorMessage: '',
          movies: [],
          totalResults: 0,
        },
      },
    }, {
      type: SEARCH_SUCCESS,
      payload: {
        searchTerm,
        data: fakeData,
      },
    })).toEqual({
      bySearchTerm: {
        [searchTerm]: {
          isLoading: false,
          error: false,
          errorMessage: '',
          movies: fakeData.Search,
          totalResults: 42,
        },
      },
      searchTerm: '',
      searchInput: '',
    });
  });

  it('should handle a failed fetch', () => {
    const searchTerm = 'Some Movie';
    const fakeErrorMessage = 'error message';

    expect(reducer({
      bySearchTerm: {
        [searchTerm]: {
          isLoading: true,
          error: false,
          errorMessage: '',
          movies: [],
          totalResults: 0,
        },
      },
    }, {
      type: SEARCH_FAILURE,
      payload: {
        searchTerm,
        error: new Error(fakeErrorMessage),
      },
    })).toEqual({
      bySearchTerm: {
        [searchTerm]: {
          isLoading: false,
          error: true,
          errorMessage: fakeErrorMessage,
          movies: [],
          totalResults: 0,
        },
      },
      searchTerm: '',
      searchInput: '',
    });
  });

  it('should handle a search term update', () => {
    const newSearch = 'new search';

    expect(reducer({
      searchTerm: '',
      searchInput: '',
      bySearchTerm: {},
    }, {
      type: SEARCH_TERM_UPDATE,
      payload: newSearch,
    })).toEqual({
      searchTerm: newSearch,
      searchInput: '',
      bySearchTerm: {},
    });
  });

  it('should handle a search input update', () => {
    const newSearch = 'new search';

    expect(reducer({
      searchInput: '',
      searchTerm: '',
      bySearchTerm: {},
    }, {
      type: SEARCH_INPUT_UPDATE,
      payload: newSearch,
    })).toEqual({
      searchInput: newSearch,
      searchTerm: '',
      bySearchTerm: {},
    });
  });
});
