import axios from 'axios';
import query from 'querystring';
import { combineReducers } from 'redux';
import { createSelector } from 'reselect';

// Action Types
export const FETCH_REQUEST = 'omdbapp/movies/FETCH_REQUEST';
export const FETCH_SUCCESS = 'omdbapp/movies/FETCH_SUCCESS';
export const FETCH_FAILURE = 'omdbapp/movies/FETCH_FAILURE';

// State Shape
export const initialState = {
  byImdbID: {},
};

const initialMovieState = {
  isLoading: true,
  error: false,
  errorMessage: '',
  movieData: {},
};

function movieReducer(state = initialMovieState, action) {
  const { type, payload } = action;

  switch (type) {
    case FETCH_REQUEST: {
      return {
        ...state,
        isLoading: true,
        error: false,
        errorMessage: '',
      };
    }
    case FETCH_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        error: false,
        errorMessage: '',
        movieData: payload.data,
      };
    }
    case FETCH_FAILURE: {
      return {
        ...state,
        isLoading: false,
        error: true,
        errorMessage: payload.error.message,
      };
    }
    default: {
      return state;
    }
  }
}

function byImdbIDReducer(state = initialState.byImdbID, action) {
  const { type, payload } = action;

  switch (type) {
    case FETCH_REQUEST:
    case FETCH_SUCCESS:
    case FETCH_FAILURE: {
      return {
        ...state,
        [payload.imdbID]: movieReducer(state[payload.imdbID], action),
      };
    }
    default: {
      return state;
    }
  }
}

const reducer = combineReducers({
  byImdbID: byImdbIDReducer,
});

export default reducer;

// Selectors
const getLocalState = state => state.movies;

export const getByImdbID = createSelector(getLocalState, state => state.byImdbID);

export const makeGetMovieSelector = imdbID => createSelector(
  getByImdbID,
  byImdbID => byImdbID[imdbID] || initialMovieState,
);

// Actions
function startMovieFetch(imdbID) {
  return {
    type: FETCH_REQUEST,
    payload: { imdbID },
  };
}

function finishMovieFetch(imdbID, data) {
  return {
    type: FETCH_SUCCESS,
    payload: {
      imdbID,
      data,
    },
  };
}

function failMovieFetch(imdbID, error) {
  return {
    type: FETCH_FAILURE,
    payload: {
      imdbID,
      error,
    },
    error: true,
  };
}

function fetchMovie(imdbID) {
  return (dispatch) => {
    dispatch(startMovieFetch(imdbID));
    return axios.get(`http://omdbapi.com/?${query.stringify({ i: imdbID, plot: 'full' })}`)
      .then((result) => {
        if (result.data.Response === 'False') {
          throw new Error(result.data.Error);
        }
        return dispatch(finishMovieFetch(imdbID, result.data));
      })
      .catch(error => dispatch(failMovieFetch(imdbID, error)));
  };
}

function shouldFetchMovie(state, imdbID) {
  const movie = getByImdbID(state)[imdbID];

  if (!movie || movie.error) {
    return true;
  }

  return false;
}

export function fetchMovieIfNeeded(imdbID) {
  return (dispatch, getState) => {
    if (shouldFetchMovie(getState(), imdbID)) {
      return dispatch(fetchMovie(imdbID));
    }

    return Promise.resolve();
  };
}
