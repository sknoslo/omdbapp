import axios from 'axios';
import query from 'querystring';
import { combineReducers } from 'redux';
import { createSelector } from 'reselect';

// Action Types
export const SEARCH_INPUT_UPDATE = 'omdbapp/search/SEARCH_INPUT_UPDATE';
export const SEARCH_TERM_UPDATE = 'omdbapp/search/SEARCH_TERM_UPDATE';
export const SEARCH_REQUEST = 'omdbapp/search/SEARCH_REQUEST';
export const SEARCH_SUCCESS = 'omdbapp/search/SEARCH_SUCCESS';
export const SEARCH_FAILURE = 'omdbapp/search/SEARCH_FAILURE';

export const initialState = {
  searchTerm: '',
  searchInput: '',
  bySearchTerm: {},
  byImdbID: {},
};

// Reducers
function searchTermReducer(state = initialState.searchTerm, action) {
  const { type, payload } = action;

  switch (type) {
    case SEARCH_TERM_UPDATE: {
      return payload;
    }
    default: {
      return state;
    }
  }
}

function searchInputReducer(state = initialState.searchInput, action) {
  const { type, payload } = action;

  switch (type) {
    case SEARCH_INPUT_UPDATE: {
      return payload;
    }
    default: {
      return state;
    }
  }
}

const initialSearchResultsState = {
  isLoading: false,
  error: false,
  errorMessage: '',
  movies: [],
  totalResults: 0,
};

function searchResultsReducer(
  state = initialSearchResultsState,
  action,
) {
  const { type, payload } = action;

  switch (type) {
    case SEARCH_REQUEST: {
      return {
        ...state,
        isLoading: true,
        error: false,
        errorMessage: '',
      };
    }
    case SEARCH_SUCCESS: {
      const { data } = payload;

      return {
        ...state,
        isLoading: false,
        error: false,
        errorMessage: '',
        movies: data.Search,
        totalResults: parseInt(data.totalResults, 10),
      };
    }
    case SEARCH_FAILURE: {
      return {
        ...state,
        isLoading: false,
        error: true,
        errorMessage: payload.error.message,
      };
    }
    default: {
      return state;
    }
  }
}

function bySearchTermReducer(state = initialState.bySearchTerm, action) {
  const { type, payload } = action;

  switch (type) {
    case SEARCH_REQUEST:
    case SEARCH_SUCCESS:
    case SEARCH_FAILURE: {
      return {
        ...state,
        [payload.searchTerm]: searchResultsReducer(state[payload.searchTerm], action),
      };
    }
    default: {
      return state;
    }
  }
}

const reducer = combineReducers({
  searchTerm: searchTermReducer,
  searchInput: searchInputReducer,
  bySearchTerm: bySearchTermReducer,
});

export default reducer;

// Selectors
const getLocalState = state => state.search;

export const getSearchTerm = createSelector(getLocalState, state => state.searchTerm);
export const getSearchInput = createSelector(getLocalState, state => state.searchInput);
export const getBySearchTerm = createSelector(getLocalState, state => state.bySearchTerm);
export const getByImdbID = createSelector(getLocalState, state => state.byImdbID);

export const getSearchResults = createSelector(
  getBySearchTerm,
  getSearchTerm,
  (bySearchTerm, searchTerm) => bySearchTerm[searchTerm] || initialSearchResultsState,
);

// Actions
export function updateSearchInput(searchTerm) {
  return {
    type: SEARCH_INPUT_UPDATE,
    payload: searchTerm,
  };
}

function updateSearchTerm(searchTerm) {
  return {
    type: SEARCH_TERM_UPDATE,
    payload: searchTerm,
  };
}

function startMovieSearch(searchTerm) {
  return {
    type: SEARCH_REQUEST,
    payload: { searchTerm },
  };
}

function finishMovieSearch(searchTerm, data) {
  return {
    type: SEARCH_SUCCESS,
    payload: {
      searchTerm,
      data,
    },
  };
}

function failMovieSearch(searchTerm, error) {
  return {
    type: SEARCH_FAILURE,
    payload: {
      searchTerm,
      error,
    },
    error: true,
  };
}

function fetchMovies(searchTerm) {
  return (dispatch) => {
    dispatch(startMovieSearch(searchTerm));
    return axios.get(`http://omdbapi.com/?${query.stringify({ s: searchTerm })}`)
      .then((result) => {
        if (result.data.Response === 'False') {
          throw new Error(result.data.Error);
        }
        return dispatch(finishMovieSearch(searchTerm, result.data));
      })
      .catch(error => dispatch(failMovieSearch(searchTerm, error)));
  };
}

function shouldSearch(state, searchTerm) {
  const movies = getBySearchTerm(state)[searchTerm];

  if (!searchTerm) {
    return false;
  }

  if (!movies || movies.error) {
    return true;
  }

  return false;
}

export function searchIfNeeded(searchTerm) {
  return (dispatch, getState) => {
    const trimmed = searchTerm.trim();
    dispatch(updateSearchTerm(trimmed));

    if (shouldSearch(getState(), trimmed)) {
      return dispatch(fetchMovies(trimmed));
    }

    return Promise.resolve();
  };
}
