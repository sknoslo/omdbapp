import moxios from 'moxios';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import reducer, {
  initialState,
  FETCH_REQUEST,
  FETCH_SUCCESS,
  FETCH_FAILURE,
  fetchMovieIfNeeded,
} from './movies';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('async actions', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  it('creates FETCH_REQUEST and FETCH_SUCCESS actions if movie is needed', () => {
    const id = 'tt123456789';
    const movieTitle = 'Movie Title';

    const response = {
      Response: 'True',
      imdbID: id,
      Title: movieTitle,
    };

    moxios.stubRequest(/omdbapi/, {
      status: 200,
      response,
    });

    const expectedActions = [
      {
        type: FETCH_REQUEST,
        payload: {
          imdbID: id,
        },
      },
      {
        type: FETCH_SUCCESS,
        payload: {
          imdbID: id,
          data: response,
        },
      },
    ];

    const store = mockStore({ movies: initialState });
    return store.dispatch(fetchMovieIfNeeded(id))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });

  it('does not create actions if movie is not needed', () => {
    const id = 'tt123456789';

    const store = mockStore({ movies: { byImdbID: { [id]: { isLoading: false } } } });

    return store.dispatch(fetchMovieIfNeeded(id))
      .then(() => {
        expect(store.getActions()).toEqual([]);
      });
  });

  it('creates a FETCH_FAILURE action when movie is not found', () => {
    const id = 'badid';
    const errorMessage = 'Incorrect IMDb ID.';

    // The omdbapi does return 200 even when no movie was found.
    moxios.stubRequest(/omdbapi/, {
      status: 200,
      response: {
        Response: 'False',
        Error: errorMessage,
      },
    });

    const expectedActions = [
      {
        type: FETCH_REQUEST,
        payload: {
          imdbID: id,
        },
      },
      {
        type: FETCH_FAILURE,
        error: true,
        payload: {
          imdbID: id,
          error: new Error(errorMessage),
        },
      },
    ];

    const store = mockStore({ movies: initialState });

    return store.dispatch(fetchMovieIfNeeded(id))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });
});

describe('movies reducer', () => {
  it('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      byImdbID: {},
    });
  });

  it('should handle a fetch request', () => {
    const id = 'tt123456789';
    expect(reducer(undefined, {
      type: FETCH_REQUEST,
      payload: { imdbID: id },
    })).toEqual({
      byImdbID: {
        [id]: {
          isLoading: true,
          error: false,
          errorMessage: '',
          movieData: {},
        },
      },
    });
  });

  it('should handle a successful fetch', () => {
    const id = 'tt123456789';
    const fakeData = { fake: 'data' };

    expect(reducer({
      byImdbID: {
        [id]: {
          isLoading: true,
          error: false,
          errorMessage: '',
          movieData: {},
        },
      },
    }, {
      type: FETCH_SUCCESS,
      payload: {
        imdbID: id,
        data: fakeData,
      },
    })).toEqual({
      byImdbID: {
        [id]: {
          isLoading: false,
          error: false,
          errorMessage: '',
          movieData: fakeData,
        },
      },
    });
  });

  it('should handle a failed fetch', () => {
    const id = 'tt123456789';
    const fakeErrorMessage = 'error message';

    expect(reducer({
      byImdbID: {
        [id]: {
          isLoading: true,
          error: false,
          errorMessage: '',
          movieData: {},
        },
      },
    }, {
      type: FETCH_FAILURE,
      payload: {
        imdbID: id,
        error: new Error(fakeErrorMessage),
      },
    })).toEqual({
      byImdbID: {
        [id]: {
          isLoading: false,
          error: true,
          errorMessage: fakeErrorMessage,
          movieData: {},
        },
      },
    });
  });
});
