import { combineReducers } from 'redux';
import search from './modules/search';
import movies from './modules/movies';

const reducer = combineReducers({
  search,
  movies,
});

export default reducer;
