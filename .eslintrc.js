module.exports = {
  parser: 'babel-eslint',

  extends: 'airbnb',

  env: {
    browser: true,
    jest: true,
  },

  rules: {
    'linebreak-style': 'off',
  },

  settings: {
    'import/resolver': {
      webpack: {
        config: './config/webpack.config.dev.js',
      },
    },
  },
};
