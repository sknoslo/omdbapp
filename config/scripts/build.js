/* eslint-disable import/no-extraneous-dependencies, no-console */
const fs = require('fs-extra');
const webpack = require('webpack');
const paths = require('../paths');
const config = require('../webpack.config.prod');

// Clear build directory.
fs.emptyDirSync(paths.build);

// Compile.
let compiler;
try {
  compiler = webpack(config);
} catch (err) {
  console.log('Compilation failed.', err.stack || err);
  console.log();
  process.exit(1);
}

console.log('compiling');

compiler.run((err, stats) => {
  if (err) {
    console.log('Compilation failed.');
    console.error(err.stack || err);
    if (err.details) {
      console.error(err.details);
    }
    process.exit(1);
  }

  const info = stats.toJson();

  if (stats.hasErrors()) {
    console.log('Compilation failed.');
    console.error(info.errors);
  }

  if (stats.hasWarnings()) {
    console.warn(info.warnings);
  }

  console.log('Compilation finished.');

  console.log(fs.readdirSync(paths.build));

  console.log('Merging static directory.');


  // Merge static directory.
  fs.copySync(paths.static, paths.build, {
    dereference: true,
    filter: file => file !== paths.template,
  });

  console.log('Merged static directory.');

  console.log(fs.readdirSync(paths.build));
});
