const path = require('path');
const fs = require('fs');

const appDirectory = fs.realpathSync(process.cwd());

function resolve(relativePath) {
  return path.resolve(appDirectory, relativePath);
}

module.exports = {
  static: resolve('static'),
  build: resolve('public'),
  src: resolve('src'),
  template: resolve('static/index.html'),
  favicon: resolve('static/favicon.ico'),
  storePath: resolve('src/store'),
  basePath: '/omdbapp',
};
