const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const paths = require('./paths');

// Override basePath in dev mode.
paths.basePath = '/';

module.exports = {
  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:4000',
    'webpack/hot/only-dev-server',
    './src/index.jsx',
  ],

  output: {
    filename: 'static/js/bundle.js',
    publicPath: paths.baseName,
    path: paths.build,
  },

  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ['node_modules', 'shared'],
    alias: {
      store: paths.storePath,
    },
  },

  devServer: {
    host: 'localhost',
    port: 4000,
    contentBase: paths.static,
    publicPath: paths.baseName,
    compress: true,
    clientLogLevel: 'none',
    hot: true,
  },

  devtool: 'inline-source-map',

  module: {
    loaders: [
      {
        enforce: 'pre',
        test: /\.(js|jsx)$/,
        include: paths.src,
        loader: 'eslint-loader',
      },
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        query: {
          cacheDirectory: true,
        },
        include: paths.src,
      },
      {
        test: /\.(sass|scss)$/,
        exclude: /\.global\.(sass|scss)$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[name]_[local]__[hash:base64:5]',
            },
          },
          'sass-loader',
        ],
      },
      {
        test: /\.global\.(sass|scss)$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
            },
          },
          'sass-loader',
        ],
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: paths.template,
      favicon: paths.favicon,
    }),
    new webpack.DefinePlugin({
      BASENAME: JSON.stringify(paths.basePath),
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ],
};
