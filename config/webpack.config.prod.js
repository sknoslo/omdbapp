const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');
const paths = require('./paths');

const jsOutFile = 'static/js/[name].[chunkhash:8].js';
const jsChunkFile = jsOutFile.replace('.js', '.chunk.js');
const cssOutFile = 'static/css/[name].[contenthash:8].css';

module.exports = {
  entry: [
    './src/index.jsx',
  ],

  output: {
    filename: jsOutFile,
    chunkFilename: jsChunkFile,
    publicPath: paths.basePath,
    path: paths.build,
  },

  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ['node_modules', 'shared'],
    alias: {
      store: paths.storePath,
    },
  },

  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        include: paths.src,
      },
      {
        test: /\.(sass|scss)$/,
        exclude: /\.global\.(sass|scss)$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'css-loader?modules=true&importLoaders=1&localIdentName=[name]_[local]__[hash:base64:5]',
            'sass-loader',
          ],
        }),
      },
      {
        test: /\.global\.(sass|scss)$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'css-loader?importLoaders=1',
            'sass-loader',
          ],
        }),
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: paths.template,
      favicon: paths.favicon,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
    }),
    new webpack.DefinePlugin({
      BASENAME: JSON.stringify(paths.basePath),
    }),
    new ExtractTextPlugin({
      filename: cssOutFile,
      allChunks: true,
    }),
  ],
};
